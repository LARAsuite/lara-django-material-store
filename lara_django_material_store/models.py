"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material_store models *

:details: lara_django_material_store database models.
         -
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - remove unwanted models/fields
________________________________________________________________________
"""

import logging
import datetime
import uuid
from random import randint

from django.utils import timezone
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from django.db import models

from lara_django_base.models import DataType, MediaType, ExtraDataAbstr, Location, CalendarAbstract, Namespace, Tag
from lara_django_people.models import Entity, LaraUser
from lara_django_material.models import Part, Device, DeviceSetup, Labware

from lara_django_store.models import ItemInstanceAbstr, OrderItemAbstr, ItemOrderAbstr, ItemBasketAbstr


settings.FIXTURES += []


class ExtraData(ExtraDataAbstr):
    """This class can be used to extend data, by extra information,
       e.g. more telephone numbers, customer numbers, ... """

    extra_data_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    file = models.FileField(
        upload_to='material_store', blank=True, null=True, help_text="rel. path/filename")


class PartInstance(ItemInstanceAbstr):
    """ Part Instance Model ...
    :param ItemInstanceAbstr: _description_
    :type ItemInstanceAbstr: _type_
    """
    part_inst_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    part = models.ForeignKey(Part, related_name='%(app_label)s_%(class)s_materials_related',
                             related_query_name="%(app_label)s_%(class)s_materials_related_query",
                             on_delete=models.CASCADE)
    name_DNS = models.TextField(
        blank=True, help_text="DNS-Domain Name Service name of part or device")
    name_MAC = models.TextField(
        blank=True, help_text="MAC address of part or device")
    IP4_static = models.TextField(
        blank=True, help_text="static IP4 address of part or device")
    IP6_static = models.TextField(
        blank=True, help_text="static IP6 address of part or device")

    operators = models.ManyToManyField(Entity, related_name="%(app_label)s_%(class)s_operators_related",
                                       related_query_name="%(app_label)s_%(class)s_operators_related_query",
                                       blank=True,
                                       help_text="people, who are instructed to operate this part or device")
    service_group = models.ManyToManyField(Entity, related_name="%(app_label)s_%(class)s_service_groups_related",
                                           related_query_name="%(app_label)s_%(class)s_service_groups_related_query",
                                           blank=True, help_text="people, who service this part/device")
    literature = models.URLField(blank=True, null=True, help_text="URL to literature regarding this part instance")
    # literature = models.ManyToManyField(LibItem, related_name="%(app_label)s_%(class)s_literature_related",
    #                                     related_query_name="%(app_label)s_%(class)s_literature_related_query",  blank=True,
    #                                     help_text="literature regarding this part instance")

    image = models.ImageField(upload_to='material_store/', blank=True, default="",
                              help_text="rel. path/filename to image")
    tags = models.ManyToManyField(Tag, blank=True, related_name='%(app_label)s_%(class)s_tags_related',
                                  related_query_name="%(app_label)s_%(class)s_tags_related_query",
                                  help_text="tags")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    def save(self, force_insert=None, using=None):
        """ Here we generate some default values for full_name """

        if self.name is None or self.name == "":
            self.name = self.part.name_full
        if self.namespace is not None:
            self.name_full = f"{self.namespace.name}/" + "_".join((self.name.replace(' ', '_') , self.registration_no, self.barcode1D))
        else:
            self.name_full = "_".join((self.name.replace(' ', '_') , self.registration_no, self.barcode1D)) 
            
        super().save(force_insert=force_insert, using=using)


            

# _______________ Ordering / Wishlists ______________


class PartOrderItem(OrderItemAbstr):
    """ Part Order Item Model for ordering parts ...
    :param OrderItemAbstr: _description_
    :type OrderItemAbstr: _type_
    """
    part_orderitem_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    part_inst = models.ForeignKey(PartInstance, related_name='%(app_label)s_%(class)s_parts_instance_related',
                                  related_query_name="%(app_label)s_%(class)s_parts_instance_related_query",
                                  on_delete=models.CASCADE)
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_oi_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_oi_extra_data_related_query",
                                        help_text="extra data")

    def __str__(self):
        return self.part.name_full or ""

    def __repr__(self):
        return self.part.name_full or ""


class PartWishlist(ItemBasketAbstr):
    """ Part Wishlist Model for wishing parts ...
    :param ItemWishlistAbstr: _description_
    :type ItemWishlistAbstr: _type_
    """
    part_wishlist_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    part_wish = models.ManyToManyField(PartOrderItem, related_name='%(app_label)s_%(class)s_part_wish_related',
                                       related_query_name="%(app_label)s_%(class)s_part_wish_related_query",
                                       blank=True, help_text="parts wished (type: OrderParts)")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""


class PartBasket(ItemBasketAbstr):
    """Part Basket Model for ordering parts and put them into a basket """
    part_basket_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    parts = models.ManyToManyField(PartOrderItem, related_name='%(app_label)s_%(class)s_parts_basket_related',
                                   related_query_name="%(app_label)s_%(class)s_parts_basket_related_query",
                                   blank=True, help_text="parts in basket")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""


class PartsOrder(ItemOrderAbstr):
    """ Order Parts Model ...
    :param ItemOrderAbstr: _description_
    :type ItemOrderAbstr: _type_
    """
    parts_order_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    part_order_items = models.ManyToManyField(PartOrderItem, related_name='%(app_label)s_%(class)s_parts_order_related',
                                              related_query_name="%(app_label)s_%(class)s_parts_order_related_query",
                                              blank=True, help_text="parts ordered")
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_po_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_po_extra_data_related_query",
                                        help_text="extra data")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    # class Meta:
        # ordering = ['-date_created']
        # verbose_name = "Order Parts"
        #verbose_name_plural = "Order Parts"


class PartLocalStore(ItemBasketAbstr):
    """ Part Local Store Model ...
        this can be used to store parts in a local store, e.g. in a lab"""
    part_localstore_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    part_instances = models.ManyToManyField(PartInstance, related_name='%(app_label)s_%(class)s_parts_inst_loc_related',
                                            related_query_name="%(app_label)s_%(class)s_parts_inst_loc_related_query",
                                            blank=True, help_text="parts in local store")
    location = models.ForeignKey(Location, related_name='%(app_label)s_%(class)s_parts_loc_related',
                                 related_query_name="%(app_label)s_%(class)s_parts_loc_related_query",
                                 on_delete=models.CASCADE)

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""


# ___________________  Devices ______________________
#

class DeviceInstance(ItemInstanceAbstr):
    """ Device Instance Model
    :param ItemInstanceAbstr: _description_
    :type ItemInstanceAbstr: _type_
    """
    device_inst_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    device = models.ForeignKey(Device, related_name='%(app_label)s_%(class)s_device_instance_related',
                               related_query_name="%(app_label)s_%(class)s_device_instance_related_query",
                               on_delete=models.CASCADE)
    name_DNS = models.TextField(blank=True, help_text="DNS-Domain Name Service name of part or device")
    name_MAC = models.TextField(blank=True, help_text="MAC address of part or device")
    IP4_static = models.TextField(blank=True, help_text="static IP4 address of part or device")
    IP6_static = models.TextField(blank=True, help_text="static IP6 address of part or device")

    operators = models.ManyToManyField(Entity, related_name="%(app_label)s_%(class)s_operators_related",
                                       related_query_name="%(app_label)s_%(class)s_operators_related_query",
                                       blank=True, help_text="people, who are instructed to operate this part or device")
    service_group = models.ManyToManyField(Entity, related_name="%(app_label)s_%(class)s_service_groups_related",
                                           related_query_name="%(app_label)s_%(class)s_service_groups_related_query",
                                           blank=True, help_text="people, who service this part/device")
    literature = models.URLField(blank=True, null=True, help_text="URL to literature regarding this part instance")
    # literature = models.ManyToManyField(LibItem, related_name="%(app_label)s_%(class)s_literature_related",
    #                                     related_query_name="%(app_label)s_%(class)s_literature_related_query",  blank=True,
    #                                     help_text="literature regarding this part instance")

    image = models.ImageField(upload_to='material_store/', blank=True, default="",
                              help_text="rel. path/filename to image")
    tags = models.ManyToManyField(Tag, blank=True, related_name='%(app_label)s_%(class)s_tags_related',
                                  related_query_name="%(app_label)s_%(class)s_tags_related_query",
                                  help_text="tags")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    def save(self, force_insert=None, using=None):
        """ Here we generate some default values for full_name """
        if self.name is None or self.name == "":
            self.name = self.device.name_full
        if self.namespace is not None:
            self.name_full = f"{self.namespace.name}/" + "_".join((self.name.replace(' ', '_') , self.registration_no, self.barcode1D))
        else:
            self.name_full = "_".join((self.name.replace(' ', '_') , self.registration_no, self.barcode1D)) 

        super(DeviceInstance, self).save(force_insert=force_insert, using=using)


class DeviceOrderItem(OrderItemAbstr):
    """ Device Order Item Model for storing device-order-items, """
    device_order_item_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    device = models.ForeignKey(DeviceInstance, related_name='%(app_label)s_%(class)s_device_oi_related',
                               related_query_name="%(app_label)s_%(class)s_device_oi_related_query",
                               on_delete=models.CASCADE)
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_oi_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_oi_extra_data_related_query",
                                        help_text="additional data for this order item")

    def __str__(self):
        return self.device.name_full or ""

    def __repr__(self):
        return self.device.name_full or ""

    class Meta:
        # ordering = ['-date_created']
        # verbose_name = "Device Order Item"
        verbose_name_plural = "Device Order Items"


class DeviceWishlist(ItemBasketAbstr):
    """ Device Wishlist Model for storing device-wishlists, """
    device_wishlist_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    devices_wish = models.ManyToManyField(DeviceOrderItem, related_name='%(app_label)s_%(class)s_dev_wish_related',
                                          related_query_name="%(app_label)s_%(class)s_dev_wish_related_query",
                                          blank=True, help_text="devices in wishlist")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    class Meta:
        # ordering = ['-date_created']
        # verbose_name = "Device Wishlist"
        verbose_name_plural = "Device Wishlists"


class DeviceBasket(ItemBasketAbstr):
    """ Device Basket Model for storing device-baskets, """
    device_basket_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    devices = models.ManyToManyField(DeviceOrderItem, related_name='%(app_label)s_%(class)s_dev_order_basket_related',
                                     related_query_name="%(app_label)s_%(class)s_dev_order_basket_related_query",
                                     blank=True, help_text="devices in basket")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    class Meta:
        # ordering = ['-date_created']
        # verbose_name = "Device Basket"
        verbose_name_plural = "Device Baskets"


class DeviceOrder(ItemOrderAbstr):
    """ Device Order Model for storing device-orders, """
    device_order_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    device_order_items = models.ManyToManyField(DeviceOrderItem, related_name='%(app_label)s_%(class)s_do_device_order_items_related',
                                                related_query_name="%(app_label)s_%(class)s_do_device_order_items_query",
                                                blank=True, help_text="devices in order")
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_devo_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_devo_extra_data_related_query",
                                        help_text="additional data for this order")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    # class Meta:
        # ordering = ['-date_created']
        # verbose_name = "Order Parts"
        # verbose_name_plural = "Device Order Parts"


class DeviceLocalStore(ItemBasketAbstr):
    """ Device LocalStore Model for storing device-local-stores,
        e.g. a local store of a lab, a company, a university, etc.
    """
    device_localstore_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    device_instances = models.ManyToManyField(DeviceInstance, related_name='%(app_label)s_%(class)s_dev_instances_related',
                                              related_query_name="%(app_label)s_%(class)s_dev_instances_related_query",
                                              blank=True, help_text="parts in local store")
    location = models.ForeignKey(Location, related_name='%(app_label)s_%(class)s_location_related',
                                 related_query_name="%(app_label)s_%(class)s_location_related_query",
                                 on_delete=models.CASCADE)

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""


class DeviceSetupInstance(ItemInstanceAbstr):
    """ DeviceSetup Instance Model for storing device-setups, like, e.g. a robotic platform, a distillation setup, etc."""
    device_setup_inst_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    device_setup = models.ForeignKey(DeviceSetup, related_name='%(app_label)s_%(class)s_device_setups_related',
                                     related_query_name="%(app_label)s_%(class)s_device_setups_related_query",
                                     on_delete=models.CASCADE)


class DeviceGroup(models.Model):
    """ device group for combining/pooling devices
        e.g. robotic platform with many devices
    """
    device_group_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    namespace = models.ForeignKey(Namespace, related_name='%(app_label)s_%(class)s_namespaces_related',
                                  related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="namespace of device group")
    name_full = models.TextField(
        unique=True, help_text="name of device group ")  # group name
    devices = models.ManyToManyField(
        DeviceInstance, related_name='%(app_label)s_%(class)s_devices_related',
        related_query_name="%(app_label)s_%(class)s_devices_related_query", blank=True)
    # colour for better visual representation, e.g. in calendars ...
    colour = models.TextField(blank=True)
    # libitems or extradata
    # ~ metainfo = models.ManyToManyField('lara_metainfo.MetaInfo', related_name='%(app_label)s_%(class)s_metainfo_related',
    # ~ related_query_name="%(app_label)s_%(class)s_metainfo", blank=True) # specifications, maintanance plan, maintanance history, incidents history, network settings, SOPs, image filename
    description = models.TextField(
        blank=True, help_text="description of device group")

    def __str__(self):
        return self.name_full or ""

    class Meta:
        db_table = "lara_devices_device_group"


class LabwareState(models.Model):
    """ labware states, e.g. init, waiting, in -use, ready, processed, discarded/trashed"""
    state_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    state = models.TextField(
        unique=True, help_text="labware states, e.g. init, waiting, in-use, ready, processed,  ")
    description = models.TextField(
        null=True, blank=True, help_text="labware state description")

    def __str__(self):
        return self.state or ''


class LabwareInstance(ItemInstanceAbstr):
    """Labware Instance Model, e.g. a part, a device, a labware, ...
    : param ItemInstanceAbstr: _description_
    : type ItemInstanceAbstr: _type_
    """
    labware_inst_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    labware = models.ForeignKey(Labware, related_name='%(app_label)s_%(class)s_labware_instance_related',
                                related_query_name="%(app_label)s_%(class)s_labware_instance_related_query",
                                on_delete=models.CASCADE)
    labware_state = models.ForeignKey(LabwareState, related_name='%(app_label)s_%(class)s_labware_state_related',
                                      related_query_name="%(app_label)s_%(class)s_labware_state_related_query",   on_delete=models.CASCADE, null=True, blank=True,
                                      help_text="labware state, e.g. init, waiting, in-use, ready, processed, discarded/trashed")
    discarded = models.BooleanField(default=False,
                                    help_text="Is labware discarded/trashed ? related to labware state, for fast selection")

    literature = models.URLField(blank=True, null=True, help_text="literature regarding this labware instance")
    # literature = models.ManyToManyField(LibItem, related_name="%(app_label)s_%(class)s_literature_related",
    #                                     related_query_name="%(app_label)s_%(class)s_literature_related_query",  blank=True,
    #                                     help_text="literature regarding this part instance")
    image = models.ImageField(upload_to='material_store/', blank=True, default="",
                              help_text="rel. path/filename to image")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    def save(self, force_insert=None, using=None):
        """ Here we generate some default values for full_name """
        if self.name is None or self.name == "":
            self.name = self.labware.name_full
        if self.namespace is not None:
            self.name_full = f"{self.namespace.name}/" + "_".join((self.name.replace(' ', '_') , self.registration_no, self.barcode1D))
        else:
            self.name_full = "_".join((self.name.replace(' ', '_') , self.registration_no, self.barcode1D)) or ""
        super(LabwareInstance, self).save(force_insert=force_insert, using=using)


class LabwareOrderItem(OrderItemAbstr):
    """ Labware Order Item Model for storing device-order-items, """
    labware_order_item_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    labware = models.ForeignKey(LabwareInstance, related_name='%(app_label)s_%(class)s_li_labware_related',
                                related_query_name="%(app_label)s_%(class)s_li_labware_related_query",
                                on_delete=models.CASCADE)
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_li_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_li_extra_data_related_query",
                                        help_text="additional data for this order item")

    def __str__(self):
        return self.labware.name_full or ""

    def __repr__(self):
        return self.labware.name_full or ""

    class Meta:
        # ordering = ['-date_created']
        # verbose_name = "Labware Order Item"
        verbose_name_plural = "Labware Order Items"


class LabwareWishlist(ItemBasketAbstr):
    """ Labware Wishlist Model for storing device-wishlists, """
    device_wishlist_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    labware_wish = models.ManyToManyField(LabwareOrderItem, related_name='%(app_label)s_%(class)s_labware_wish_related',
                                          related_query_name="%(app_label)s_%(class)s_labware_wish_related_query",
                                          blank=True, help_text="labware in wishlist")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    class Meta:
        # ordering = ['-date_created']
        # verbose_name = "Labware Wishlist"
        verbose_name_plural = "Labware Wishlists"


class LabwareBasket(ItemBasketAbstr):
    """ Labware Basket Model for storing labware-baskets, """
    device_basket_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    labware = models.ManyToManyField(LabwareOrderItem, related_name='%(app_label)s_%(class)s_labware_basket_related',
                                     related_query_name="%(app_label)s_%(class)s_labware_basket_related_query",
                                     blank=True, help_text="labware in basket")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    class Meta:
        # ordering = ['-date_created']
        # verbose_name = "Labware Basket"
        verbose_name_plural = "Labware Baskets"


class LabwareOrder(ItemOrderAbstr):
    """ Labware Order Model for storing labware-orders, """
    labware_order_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    labware_order_items = models.ManyToManyField(LabwareOrderItem, related_name='%(app_label)s_%(class)s_labware_order_related',
                                                 related_query_name="%(app_label)s_%(class)s_labware_order_related_query",
                                                 blank=True, help_text="labware in order")
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_lo_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_lo_extra_data_related_query",
                                        help_text="additional data for this order")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    # class Meta:
        # ordering = ['-date_created']


class LabwareLocalStore(ItemBasketAbstr):
    """ Labware LocalStore Model for storing labware-local-stores,
        e.g. a local store of a lab, a company, a university, etc.
    """
    labware_localstore_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    labware_instances = models.ManyToManyField(LabwareInstance, related_name='%(app_label)s_%(class)s_labware_inst_loc_related',
                                               related_query_name="%(app_label)s_%(class)s_labware_inst_loc_related_query",
                                               blank=True, help_text="parts in local store")
    location = models.ForeignKey(Location, related_name='%(app_label)s_%(class)s_labware_location_loc_related',
                                 related_query_name="%(app_label)s_%(class)s_labware_location_loc_related_query",
                                 on_delete=models.CASCADE)

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""


class LabwareGroup(models.Model):
    """ Labware group for combining/grouping labware"""
    labware_group_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    namespace = models.ForeignKey(Namespace, related_name='%(app_label)s_%(class)s_lg_namespaces_related',
                                  related_query_name="%(app_label)s_%(class)s_lg_namespaces_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="namespace of labware")
    name_full = models.TextField(unique=True,
                                 help_text="unique, fully qualified name of labware group, containing namespace, e.g.: de.unigreifswald.biochem.akb.lara.2333")
    labwares = models.ManyToManyField(
        LabwareInstance, related_name='%(app_label)s_%(class)s_labware_related',
        related_query_name="%(app_label)s_%(class)s_labware_related_query", blank=True)
    description = models.TextField(
        blank=True, null=True, help_text="labware group description")

    def __str__(self):
        return self.name_full or ''

    class Meta:
        db_table = "lara_labware_labware_group"


# ______________ device booking ______________


class DeviceBookingCalendar(CalendarAbstract):
    """_Device Booking Calendar_

    : param CalendarAbstract: _description_
    : type CalendarAbstract: _type_
    """
    calendar_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(LaraUser, related_name="%(app_label)s_%(class)s_user_related",
                             on_delete=models.CASCADE, null=True, blank=True,  help_text="LARA User, who created the calendar entry")
    attendees = models.ManyToManyField(LaraUser, related_name='%(app_label)s_%(class)s_attendees_devicebooking_related',
                                       related_query_name="%(app_label)s_%(class)s_attendees_devicebooking_related_query", blank=True,
                                       help_text="attendees of the meeting")
    device = models.ForeignKey(DeviceInstance, related_name="%(app_label)s_%(class)s_device_related",
                               related_query_name="%(app_label)s_%(class)s_device_related_query",
                               on_delete=models.CASCADE, null=True, blank=True,  help_text="Device to be booked")
