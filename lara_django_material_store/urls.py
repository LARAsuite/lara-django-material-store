"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material_store urls *

:details: lara_django_material_store urls module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from django.views.generic import TemplateView
#import lara_django.urls as base_urls

from . import views

# Add your {cookiecutter.project_slug}} urls here.


# !! this sets the apps namespace to be used in the template
app_name = "lara_django_material_store"

# companies and institutions should also be added
# the 'name' attribute is used in templates to address the url independent of the view


urlpatterns = [
    path('part/list/', views.PartSingleTableView.as_view(), name='part-list'),
    path('part/create/', views.PartCreateView.as_view(), name='part-create'),
    path('part/update/<uuid:pk>', views.PartUpdateView.as_view(), name='part-update'),
    path('part/delete/<uuid:pk>', views.PartDeleteView.as_view(), name='part-delete'),
    path('part/<uuid:pk>/', views.PartDetailView.as_view(), name='part-detail'),
    path('device/list/', views.DeviceSingleTableView.as_view(), name='device-list'),
    path('device/create/', views.DeviceCreateView.as_view(), name='device-create'),
    path('device/update/<uuid:pk>', views.DeviceUpdateView.as_view(), name='device-update'),
    path('device/delete/<uuid:pk>', views.DeviceDeleteView.as_view(), name='device-delete'),
    path('device/<uuid:pk>/', views.DeviceDetailView.as_view(), name='device-detail'),
    path('labware/list/', views.LabwareSingleTableView.as_view(), name='labware-list'),
    path('labware/create/', views.LabwareCreateView.as_view(), name='labware-create'),
    path('labware/update/<uuid:pk>', views.LabwareUpdateView.as_view(), name='labware-update'),
    path('labware/delete/<uuid:pk>', views.LabwareDeleteView.as_view(), name='labware-delete'),
    path('labware/<uuid:pk>/', views.LabwareDetailView.as_view(), name='labware-detail'),
    path('', views.PartSingleTableView.as_view(), name='material-store-root'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
