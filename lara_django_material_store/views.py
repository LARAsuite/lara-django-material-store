"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material_store_store views *

:details: lara_django_material_store_store views module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from dataclasses import dataclass, field
from typing import List
import datetime

from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView

from django_tables2 import SingleTableView

from .models import PartInstance, DeviceInstance, LabwareInstance

from .forms import PartInstanceCreateForm, PartInstanceUpdateForm, DeviceInstanceCreateForm, DeviceInstanceUpdateForm, LabwareInstanceCreateForm, LabwareInstanceUpdateForm
from .tables import PartInstanceTable, DeviceInstanceTable, LabwareInstanceTable

# Create your  lara_django_material_store_store views here.


@dataclass
class MaterialStoreMenu:
    menu_items:  List[dict] = field(default_factory=lambda: [
        {'name': 'Part',
         'path': 'lara_django_material:part-list'},
        {'name': 'Part-Store',
                 'path': 'lara_django_material_store:part-list'},
        {'name': 'Devices',
         'path': 'lara_django_material:device-list'},
         {'name': 'Devices-Store',
         'path': 'lara_django_material_store:device-list'},
        {'name': 'Labware',
          'path': 'lara_django_material:labware-list'},
        {'name': 'Labware-Store',
          'path': 'lara_django_material_store:labware-list'}  
    ])


class PartSingleTableView(SingleTableView):
    model = PartInstance
    table_class = PartInstanceTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'part_id', 'part_class', 'shape')

    template_name = 'lara_django_material_store/list.html'
    success_url = '/material-store/part/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Part Store - List"
        context['create_link'] = 'lara_django_material_store:part-create'
        context['menu_items'] = MaterialStoreMenu().menu_items
        return context


class PartDetailView(DetailView):
    model = PartInstance

    template_name = 'lara_django_material_store/part_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Part Store - Details"
        context['update_link'] = 'lara_django_material_store:part-update'
        context['menu_items'] = MaterialStoreMenu().menu_items
        return context


class PartCreateView(CreateView):
    model = PartInstance

    template_name = 'lara_django_material_store/create_form.html'
    form_class = PartInstanceCreateForm
    success_url = '/material-store/part/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Part Store - Create"
        return context


class PartUpdateView(UpdateView):
    model = PartInstance

    template_name = 'lara_django_material_store/update_form.html'
    form_class = PartInstanceUpdateForm
    success_url = '/material-store/part/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Part Store - Update"
        context['delete_link'] = 'lara_django_material_store:part-delete'
        return context


class PartDeleteView(DeleteView):
    model = PartInstance

    template_name = 'lara_django_material_store/delete_form.html'
    success_url = '/material-store/part/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Part Store - Delete"
        context['delete_link'] = 'lara_django_material_store:part-delete'
        return context


class DeviceSingleTableView(SingleTableView):
    model = DeviceInstance
    table_class = DeviceInstanceTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'part_id', 'part_class', 'shape')

    template_name = 'lara_django_material_store/list.html'
    success_url = '/material-store/device/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Device Store - List"
        context['create_link'] = 'lara_django_material_store:device-create'
        context['menu_items'] = MaterialStoreMenu().menu_items
        return context


class DeviceDetailView(DetailView):
    model = DeviceInstance

    template_name = 'lara_django_material_store/part_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Device Store - Details"
        context['update_link'] = 'lara_django_material_store:device-update'
        context['menu_items'] = MaterialStoreMenu().menu_items
        return context


class DeviceCreateView(CreateView):
    model = DeviceInstance

    template_name = 'lara_django_material_store/create_form.html'
    form_class = DeviceInstanceCreateForm
    success_url = '/material-store/device/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Device Store - Create"
        return context


class DeviceUpdateView(UpdateView):
    model = DeviceInstance

    template_name = 'lara_django_material_store/update_form.html'
    form_class = DeviceInstanceUpdateForm
    success_url = '/material-store/device/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Device Store - Update"
        context['delete_link'] = 'lara_django_material_store:device-delete'
        return context


class DeviceDeleteView(DeleteView):
    model = DeviceInstance

    template_name = 'lara_django_material_store/delete_form.html'
    success_url = '/material-store/device/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Device Store - Delete"
        context['delete_link'] = 'lara_django_material_store:device-delete'
        return context


class LabwareSingleTableView(SingleTableView):
    model = LabwareInstance
    table_class = LabwareInstanceTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'part_id', 'part_class', 'shape')

    template_name = 'lara_django_material_store/list.html'
    success_url = '/material-store/labware/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Labware Store - List"
        context['create_link'] = 'lara_django_material_store:labware-create'
        context['menu_items'] = MaterialStoreMenu().menu_items
        return context


class LabwareDetailView(DetailView):
    model = LabwareInstance

    template_name = 'lara_django_material_store/part_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Labware Store - Details"
        context['update_link'] = 'lara_django_material_store:labware-update'
        context['menu_items'] = MaterialStoreMenu().menu_items
        return context


class LabwareCreateView(CreateView):
    model = LabwareInstance

    template_name = 'lara_django_material_store/create_form.html'
    form_class = LabwareInstanceCreateForm
    success_url = '/material-store/labware/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Labware Store - Create"
        return context


class LabwareUpdateView(UpdateView):
    model = LabwareInstance

    template_name = 'lara_django_material_store/update_form.html'
    form_class = LabwareInstanceUpdateForm
    success_url = '/material-store/labware/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Labware Store - Update"
        context['delete_link'] = 'lara_django_material_store:labware-delete'
        return context


class LabwareDeleteView(DeleteView):
    model = LabwareInstance

    template_name = 'lara_django_material_store/delete_form.html'
    success_url = '/material-store/labware/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Labware Store - Delete"
        context['delete_link'] = 'lara_django_material_store:labware-delete'
        return context



class DeviceCalendar(DetailView):
    model = DeviceInstance  # CalendarEvent

    # ~ def get_object(self, queryset=None):
    # ~ curr_obj = super().get_object(queryset=queryset)
    # ~ return curr_obj

    template_name = 'lara_devices/device_calendar_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        event_list = ""

        calendar_options = """{{ defaultView: 'agendaWeek',
                        timeFormat: "HH:mm",
                        dateFormat: "Y/m/d",
                        header: {{
                            left: 'prev,next today',
                            center: 'title',
                            right: 'agendaWeek,agendaDay,month,',
                        }},
                        allDaySlot: false,
                        firstDay: 0,
                        weekMode: 'liquid',
                        slotMinutes: 15,
                        defaultEventMinutes: 30,
                        minTime: 8,
                        maxTime: 20,
                        editable: true,
                        eventLimit: true, // allow "more" link when too many events
                        events: [{calendar_events}],
                        dayClick: function(date, allDay, jsEvent, view) {{
                            if (allDay) {{       
                                $('#calendar').fullCalendar('gotoDate', date)      
                                $('#calendar').fullCalendar('changeView', 'agendaDay')
                            }}
                        }},
                        eventClick: function(event, jsEvent, view) {{
                            if (view.name == 'month') {{    
                                $('#calendar').fullCalendar('gotoDate', event.start)      
                                $('#calendar').fullCalendar('changeView', 'agendaDay')
                            }}
                        }},
                    }}"""

        curr_device = self.object

        booking_events_qs = curr_device.booking_events.all()

        if booking_events_qs:
            event_list = ",".join([bk_event.JSON for bk_event in booking_events_qs])

        #~ sys.stderr.write("ev_list =:{}\n".format( event_list ))

        # !! use the safe tag in the template to render this string !!!
        context['calendar_options'] = calendar_options.strip().format(calendar_events=event_list)
        return context

# ------- proof of concept stuff -------


def currentDatetime(request):
    now = datetime.datetime.now()
    html = "<html> <meta http-equiv=\"refresh\" content=\"5\" /> <body>It is now %s.</body></html>" % now
    return HttpResponse(html)
