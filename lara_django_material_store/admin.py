# -*- coding: utf-8 -*-
from django.contrib import admin


from .models import ExtraData, PartInstance, PartOrderItem, PartWishlist, PartBasket, PartsOrder, PartLocalStore, DeviceInstance, DeviceOrderItem, DeviceWishlist, DeviceBasket, DeviceOrder, DeviceLocalStore, DeviceSetupInstance, DeviceGroup, LabwareState, LabwareInstance, LabwareOrderItem, LabwareWishlist, LabwareBasket, LabwareOrder, LabwareLocalStore, LabwareGroup, DeviceBookingCalendar


@admin.register(ExtraData)
class ExtraDataAdmin(admin.ModelAdmin):
    list_display = (
        'data_type',
        'namespace',
        'URI',
        'text',
        'XML',
        'JSON',
        'bin',
        'media_type',
        'IRI',
        'URL',
        'description',
        'extra_data_id',
        'file',
    )
    list_filter = ('data_type', 'namespace', 'media_type')


@admin.register(PartInstance)
class PartInstanceAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name',
        'name_full',
        'barcode1D',
        'barcode2D',
        'UUID',
        'URL',
        'handle',
        'IRI',
        'registration_no',
        'vendor',
        'product_id',
        'serial_no',
        'service_no',
        'manufacturing_date',
        'price',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'hash_SHA256',
        'description',
        'part_inst_id',
        'part',
        'name_DNS',
        'name_MAC',
        'IP4_static',
        'IP6_static',
        'literature',
        'image',
    )
    list_filter = (
        'namespace',
        'vendor',
        'manufacturing_date',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'part',
    )
    raw_id_fields = ('operators', 'service_group', 'tags')
    search_fields = ('name',)


@admin.register(PartOrderItem)
class PartOrderItemAdmin(admin.ModelAdmin):
    list_display = (
        'quantity',
        'item_price_net',
        'item_price_gross',
        'item_shipping_price',
        'item_price_tax',
        'item_toll',
        'toll_number',
        'item_discount',
        'total_price_net',
        'part_orderitem_id',
        'part_inst',
    )
    list_filter = ('part_inst',)
    raw_id_fields = ('extra_data',)


@admin.register(PartWishlist)
class PartWishlistAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'part_wishlist_id',
    )
    list_filter = ('namespace', 'user', 'date_created', 'date_modified')
    raw_id_fields = ('part_wish',)


@admin.register(PartBasket)
class PartBasketAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'part_basket_id',
    )
    list_filter = ('namespace', 'user', 'date_created', 'date_modified')
    raw_id_fields = ('parts',)


@admin.register(PartsOrder)
class PartsOrderAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'IRI',
        'user_ordered',
        'user_ordering',
        'order_date',
        'order_id_internal',
        'order_id_external',
        'order_barcode',
        'order_company',
        'order_quote_id',
        'order_quote_date',
        'order_invoice_id',
        'order_invoice_date',
        'shipping_company',
        'shipping_tracking_id',
        'shipping_price',
        'order_total_price_net',
        'order_total_price_gross',
        'order_total_price_tax',
        'order_total_price_currency',
        'order_total_price_toll',
        'order_state',
        'parts_order_id',
    )
    list_filter = (
        'namespace',
        'user_ordered',
        'user_ordering',
        'order_date',
        'order_company',
        'order_quote_date',
        'order_invoice_date',
        'shipping_company',
        'order_total_price_currency',
        'order_state',
    )
    raw_id_fields = ('part_order_items', 'extra_data')


@admin.register(PartLocalStore)
class PartLocalStoreAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'part_localstore_id',
        'location',
    )
    list_filter = (
        'namespace',
        'user',
        'date_created',
        'date_modified',
        'location',
    )
    raw_id_fields = ('part_instances',)


@admin.register(DeviceInstance)
class DeviceInstanceAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name',
        'name_full',
        'barcode1D',
        'barcode2D',
        'UUID',
        'URL',
        'handle',
        'IRI',
        'registration_no',
        'vendor',
        'product_id',
        'serial_no',
        'service_no',
        'manufacturing_date',
        'price',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'hash_SHA256',
        'description',
        'device_inst_id',
        'device',
        'name_DNS',
        'name_MAC',
        'IP4_static',
        'IP6_static',
        'literature',
        'image',
    )
    list_filter = (
        'namespace',
        'vendor',
        'manufacturing_date',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'device',
    )
    raw_id_fields = ('operators', 'service_group', 'tags')
    search_fields = ('name',)


@admin.register(DeviceOrderItem)
class DeviceOrderItemAdmin(admin.ModelAdmin):
    list_display = (
        'quantity',
        'item_price_net',
        'item_price_gross',
        'item_shipping_price',
        'item_price_tax',
        'item_toll',
        'toll_number',
        'item_discount',
        'total_price_net',
        'device_order_item_id',
        'device',
    )
    list_filter = ('device',)
    raw_id_fields = ('extra_data',)


@admin.register(DeviceWishlist)
class DeviceWishlistAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'device_wishlist_id',
    )
    list_filter = ('namespace', 'user', 'date_created', 'date_modified')
    raw_id_fields = ('devices_wish',)


@admin.register(DeviceBasket)
class DeviceBasketAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'device_basket_id',
    )
    list_filter = ('namespace', 'user', 'date_created', 'date_modified')
    raw_id_fields = ('devices',)


@admin.register(DeviceOrder)
class DeviceOrderAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'IRI',
        'user_ordered',
        'user_ordering',
        'order_date',
        'order_id_internal',
        'order_id_external',
        'order_barcode',
        'order_company',
        'order_quote_id',
        'order_quote_date',
        'order_invoice_id',
        'order_invoice_date',
        'shipping_company',
        'shipping_tracking_id',
        'shipping_price',
        'order_total_price_net',
        'order_total_price_gross',
        'order_total_price_tax',
        'order_total_price_currency',
        'order_total_price_toll',
        'order_state',
        'device_order_id',
    )
    list_filter = (
        'namespace',
        'user_ordered',
        'user_ordering',
        'order_date',
        'order_company',
        'order_quote_date',
        'order_invoice_date',
        'shipping_company',
        'order_total_price_currency',
        'order_state',
    )
    raw_id_fields = ('device_order_items', 'extra_data')


@admin.register(DeviceLocalStore)
class DeviceLocalStoreAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'device_localstore_id',
        'location',
    )
    list_filter = (
        'namespace',
        'user',
        'date_created',
        'date_modified',
        'location',
    )
    raw_id_fields = ('device_instances',)


@admin.register(DeviceSetupInstance)
class DeviceSetupInstanceAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name',
        'name_full',
        'barcode1D',
        'barcode2D',
        'UUID',
        'URL',
        'handle',
        'IRI',
        'registration_no',
        'vendor',
        'product_id',
        'serial_no',
        'service_no',
        'manufacturing_date',
        'price',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'hash_SHA256',
        'literature',
        'description',
        'device_setup_inst_id',
        'device_setup',
    )
    list_filter = (
        'namespace',
        'vendor',
        'manufacturing_date',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'device_setup',
    )
    raw_id_fields = ('tags',)
    search_fields = ('name',)


@admin.register(DeviceGroup)
class DeviceGroupAdmin(admin.ModelAdmin):
    list_display = (
        'device_group_id',
        'namespace',
        'name_full',
        'colour',
        'description',
    )
    list_filter = ('namespace',)
    raw_id_fields = ('devices',)


@admin.register(LabwareState)
class LabwareStateAdmin(admin.ModelAdmin):
    list_display = ('state_id', 'state', 'description')


@admin.register(LabwareInstance)
class LabwareInstanceAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name',
        'name_full',
        'barcode1D',
        'barcode2D',
        'UUID',
        'URL',
        'handle',
        'IRI',
        'registration_no',
        'vendor',
        'product_id',
        'serial_no',
        'service_no',
        'manufacturing_date',
        'price',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'hash_SHA256',
        'description',
        'labware_inst_id',
        'labware',
        'labware_state',
        'discarded',
        'literature',
        'image',
    )
    list_filter = (
        'namespace',
        'vendor',
        'manufacturing_date',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'labware',
        'labware_state',
        'discarded',
    )
    raw_id_fields = ('tags',)
    search_fields = ('name',)


@admin.register(LabwareOrderItem)
class LabwareOrderItemAdmin(admin.ModelAdmin):
    list_display = (
        'quantity',
        'item_price_net',
        'item_price_gross',
        'item_shipping_price',
        'item_price_tax',
        'item_toll',
        'toll_number',
        'item_discount',
        'total_price_net',
        'labware_order_item_id',
        'labware',
    )
    list_filter = ('labware',)
    raw_id_fields = ('extra_data',)


@admin.register(LabwareWishlist)
class LabwareWishlistAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'device_wishlist_id',
    )
    list_filter = ('namespace', 'user', 'date_created', 'date_modified')
    raw_id_fields = ('labware_wish',)


@admin.register(LabwareBasket)
class LabwareBasketAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'device_basket_id',
    )
    list_filter = ('namespace', 'user', 'date_created', 'date_modified')
    raw_id_fields = ('labware',)


@admin.register(LabwareOrder)
class LabwareOrderAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'IRI',
        'user_ordered',
        'user_ordering',
        'order_date',
        'order_id_internal',
        'order_id_external',
        'order_barcode',
        'order_company',
        'order_quote_id',
        'order_quote_date',
        'order_invoice_id',
        'order_invoice_date',
        'shipping_company',
        'shipping_tracking_id',
        'shipping_price',
        'order_total_price_net',
        'order_total_price_gross',
        'order_total_price_tax',
        'order_total_price_currency',
        'order_total_price_toll',
        'order_state',
        'labware_order_id',
    )
    list_filter = (
        'namespace',
        'user_ordered',
        'user_ordering',
        'order_date',
        'order_company',
        'order_quote_date',
        'order_invoice_date',
        'shipping_company',
        'order_total_price_currency',
        'order_state',
    )
    raw_id_fields = ('labware_order_items', 'extra_data')


@admin.register(LabwareLocalStore)
class LabwareLocalStoreAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'labware_localstore_id',
        'location',
    )
    list_filter = (
        'namespace',
        'user',
        'date_created',
        'date_modified',
        'location',
    )
    raw_id_fields = ('labware_instances',)


@admin.register(LabwareGroup)
class LabwareGroupAdmin(admin.ModelAdmin):
    list_display = (
        'labware_group_id',
        'namespace',
        'name_full',
        'description',
    )
    list_filter = ('namespace',)
    raw_id_fields = ('labwares',)


@admin.register(DeviceBookingCalendar)
class DeviceBookingCalendarAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'calendar_URL',
        'summary',
        'description',
        'color',
        'ics',
        'start_datetime',
        'end_datetime',
        'duration',
        'all_day',
        'created',
        'last_modified',
        'timestamp',
        'location',
        'geolocation',
        'conference_URL',
        'range',
        'related',
        'role',
        'tzid',
        'offset_UTC',
        'alarm_repeat_JSON',
        'event_repeat',
        'event_repeat_JSON',
        'calendar_id',
        'user',
        'device',
    )
    list_filter = (
        'start_datetime',
        'end_datetime',
        'all_day',
        'created',
        'last_modified',
        'timestamp',
        'location',
        'geolocation',
        'user',
        'device',
    )
    raw_id_fields = ('tags', 'attendees')
