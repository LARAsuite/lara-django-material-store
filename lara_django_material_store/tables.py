"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material_store admin *

:details: lara_django_material_store admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev tables_generator lara_django_material_store >> tables.py" to update this file
________________________________________________________________________
"""

# django Tests s. https://docs.djangoproject.com/en/4.1/topics/testing/overview/ for lara_django_material_store
# generated with django-extensions tests_generator  lara_django_material_store > tests.py (LARA-version)

import logging
import django_tables2 as tables


from .models import ExtraData, PartInstance, PartOrderItem, PartWishlist, PartBasket, PartsOrder, PartLocalStore, DeviceInstance, DeviceOrderItem, DeviceWishlist, DeviceBasket, DeviceOrder, DeviceLocalStore, DeviceSetupInstance, DeviceGroup, LabwareState, LabwareInstance, LabwareOrderItem, LabwareWishlist, LabwareBasket, LabwareOrder, LabwareLocalStore, LabwareGroup, DeviceBookingCalendar

class ExtraDataTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:extradata-detail', [tables.A('pk')]))

    class Meta:
        model = ExtraData

        fields = (
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'bin',
                'media_type',
                'IRI',
                'URL',
                'description',
                'file')

class PartInstanceTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_material_store:part-detail', [tables.A('pk')]))

    class Meta:
        model = PartInstance

        fields = (
                'name',
                'namespace',
                
                'name_full',
              
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
               
                'purchase_date',
              
                'end_of_warranty_date',
                'location',
               
                'description',
                'part',
              
                'literature',
                'image')

class PartOrderItemTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:partorderitem-detail', [tables.A('pk')]))

    class Meta:
        model = PartOrderItem

        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'part_inst')

class PartWishlistTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:partwishlist-detail', [tables.A('pk')]))

    class Meta:
        model = PartWishlist

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

class PartBasketTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:partbasket-detail', [tables.A('pk')]))

    class Meta:
        model = PartBasket

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

class PartsOrderTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:partsorder-detail', [tables.A('pk')]))

    class Meta:
        model = PartsOrder

        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_date',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_pay_date',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

class PartLocalStoreTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:partlocalstore-detail', [tables.A('pk')]))

    class Meta:
        model = PartLocalStore

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

class DeviceInstanceTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_material_store:device-detail', [tables.A('pk')]))

    class Meta:
        model = DeviceInstance

        fields = (
                'name',
                'namespace',
                
                'name_full',
               
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
          

                'purchase_date',
             
                'end_of_warranty_date',
                'location',
               
                'description',
                'device',
               
                'literature',
                'image')

class DeviceOrderItemTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:deviceorderitem-detail', [tables.A('pk')]))

    class Meta:
        model = DeviceOrderItem

        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'device')

class DeviceWishlistTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:devicewishlist-detail', [tables.A('pk')]))

    class Meta:
        model = DeviceWishlist

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

class DeviceBasketTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:devicebasket-detail', [tables.A('pk')]))

    class Meta:
        model = DeviceBasket

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

class DeviceOrderTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:deviceorder-detail', [tables.A('pk')]))

    class Meta:
        model = DeviceOrder

        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_date',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_pay_date',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

class DeviceLocalStoreTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:devicelocalstore-detail', [tables.A('pk')]))

    class Meta:
        model = DeviceLocalStore

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

class DeviceSetupInstanceTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name= tables.Column(linkify=('lara_django_material_store:devicesetup-detail', [tables.A('pk')]))

    class Meta:
        model = DeviceSetupInstance

        fields = (
                'name',
                'namespace',
                
                'name_full',
                'barcode1D',
               
             
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
               
                'purchase_date',
               
                'end_of_warranty_date',
                'location',
               
                'literature',
                'description',
                'device_setup')

class DeviceGroupTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:devicegroup-detail', [tables.A('pk')]))

    class Meta:
        model = DeviceGroup

        fields = (
                'namespace',
                'name_full',
                'colour',
                'description')

class LabwareStateTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:labwarestate-detail', [tables.A('pk')]))

    class Meta:
        model = LabwareState

        fields = (
                'state',
                'description')

class LabwareInstanceTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_material_store:labware-detail', [tables.A('pk')]))

    class Meta:
        model = LabwareInstance

        fields = (
                'name',
                'namespace',
                
                'name_full',
                'barcode1D',
                
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
               
                'purchase_date',
              
                'end_of_warranty_date',
                'location',
              
                'description',
                'labware',
                'labware_state',
                'discarded',
                'literature',
                'image')

class LabwareOrderItemTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:labwareorderitem-detail', [tables.A('pk')]))

    class Meta:
        model = LabwareOrderItem

        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'labware')

class LabwareWishlistTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:labwarewishlist-detail', [tables.A('pk')]))

    class Meta:
        model = LabwareWishlist

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

class LabwareBasketTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:labwarebasket-detail', [tables.A('pk')]))

    class Meta:
        model = LabwareBasket

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

class LabwareOrderTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:labwareorder-detail', [tables.A('pk')]))

    class Meta:
        model = LabwareOrder

        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_date',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_pay_date',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

class LabwareLocalStoreTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:labwarelocalstore-detail', [tables.A('pk')]))

    class Meta:
        model = LabwareLocalStore

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

class LabwareGroupTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:labwaregroup-detail', [tables.A('pk')]))

    class Meta:
        model = LabwareGroup

        fields = (
                'namespace',
                'name_full',
                'description')

class DeviceBookingCalendarTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material_store:devicebookingcalendar-detail', [tables.A('pk')]))

    class Meta:
        model = DeviceBookingCalendar

        fields = (
                'title',
                'calendar_URL',
                'summary',
             
                'color',
                'ics',
                'start_datetime',
                'end_datetime',
                'duration',
                'all_day',
                'created',
                'last_modified',
                'timestamp',
                'location',
                'geolocation',
                'conference_URL',
                'range',
                'related',
                'role',
                'tzid',
                'offset_UTC',
                'alarm_repeat_JSON',
                'event_repeat',
                'event_repeat_JSON',
                'user',
                'device')

