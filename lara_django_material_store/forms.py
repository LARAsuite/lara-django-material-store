"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material_store admin *

:details: lara_django_material_store admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev forms_generator -c lara_django_material_store > forms.py" to update this file
________________________________________________________________________
"""

# # django crispy forms s. https://github.com/django-crispy-forms/django-crispy-forms for []
# generated with django-extensions forms_generator -c  [] > forms.py (LARA-version)

from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

from .models import ExtraData, PartInstance, PartOrderItem, PartWishlist, PartBasket, PartsOrder, PartLocalStore, DeviceInstance, DeviceOrderItem, DeviceWishlist, DeviceBasket, DeviceOrder, DeviceLocalStore, DeviceSetupInstance, DeviceGroup, LabwareState, LabwareInstance, LabwareOrderItem, LabwareWishlist, LabwareBasket, LabwareOrder, LabwareLocalStore, LabwareGroup, DeviceBookingCalendar 

class ExtraDataCreateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'media_type',
                'IRI',
                'URL',
                'description',
                'file')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'media_type',
                'IRI',
                'URL',
                'description',
                'file',
            Submit('submit', 'Create')
        )

class ExtraDataUpdateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'media_type',
                'IRI',
                'URL',
                'description',
                'file')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'media_type',
                'IRI',
                'URL',
                'description',
                'file',
            Submit('submit', 'Create')
        )

class PartInstanceCreateForm(forms.ModelForm):
    class Meta:
        model = PartInstance
        fields = (
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'part',
                'name_DNS',
                'name_MAC',
                'IP4_static',
                'IP6_static',
                'literature',
                'image')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'part',
                'name_DNS',
                'name_MAC',
                'IP4_static',
                'IP6_static',
                'literature',
                'image',
            Submit('submit', 'Create')
        )

class PartInstanceUpdateForm(forms.ModelForm):
    class Meta:
        model = PartInstance
        fields = (
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'part',
                'name_DNS',
                'name_MAC',
                'IP4_static',
                'IP6_static',
                'literature',
                'image')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'part',
                'name_DNS',
                'name_MAC',
                'IP4_static',
                'IP6_static',
                'literature',
                'image',
            Submit('submit', 'Create')
        )

class PartOrderItemCreateForm(forms.ModelForm):
    class Meta:
        model = PartOrderItem
        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'part_inst')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'part_inst',
            Submit('submit', 'Create')
        )

class PartOrderItemUpdateForm(forms.ModelForm):
    class Meta:
        model = PartOrderItem
        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'part_inst')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'part_inst',
            Submit('submit', 'Create')
        )

class PartWishlistCreateForm(forms.ModelForm):
    class Meta:
        model = PartWishlist
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class PartWishlistUpdateForm(forms.ModelForm):
    class Meta:
        model = PartWishlist
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class PartBasketCreateForm(forms.ModelForm):
    class Meta:
        model = PartBasket
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class PartBasketUpdateForm(forms.ModelForm):
    class Meta:
        model = PartBasket
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class PartsOrderCreateForm(forms.ModelForm):
    class Meta:
        model = PartsOrder
        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state',
            Submit('submit', 'Create')
        )

class PartsOrderUpdateForm(forms.ModelForm):
    class Meta:
        model = PartsOrder
        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state',
            Submit('submit', 'Create')
        )

class PartLocalStoreCreateForm(forms.ModelForm):
    class Meta:
        model = PartLocalStore
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location',
            Submit('submit', 'Create')
        )

class PartLocalStoreUpdateForm(forms.ModelForm):
    class Meta:
        model = PartLocalStore
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location',
            Submit('submit', 'Create')
        )

class DeviceInstanceCreateForm(forms.ModelForm):
    class Meta:
        model = DeviceInstance
        fields = (
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'device',
                'name_DNS',
                'name_MAC',
                'IP4_static',
                'IP6_static',
                'literature',
                'image')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'device',
                'name_DNS',
                'name_MAC',
                'IP4_static',
                'IP6_static',
                'literature',
                'image',
            Submit('submit', 'Create')
        )

class DeviceInstanceUpdateForm(forms.ModelForm):
    class Meta:
        model = DeviceInstance
        fields = (
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'device',
                'name_DNS',
                'name_MAC',
                'IP4_static',
                'IP6_static',
                'literature',
                'image')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'device',
                'name_DNS',
                'name_MAC',
                'IP4_static',
                'IP6_static',
                'literature',
                'image',
            Submit('submit', 'Create')
        )

class DeviceOrderItemCreateForm(forms.ModelForm):
    class Meta:
        model = DeviceOrderItem
        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'device')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'device',
            Submit('submit', 'Create')
        )

class DeviceOrderItemUpdateForm(forms.ModelForm):
    class Meta:
        model = DeviceOrderItem
        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'device')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'device',
            Submit('submit', 'Create')
        )

class DeviceWishlistCreateForm(forms.ModelForm):
    class Meta:
        model = DeviceWishlist
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class DeviceWishlistUpdateForm(forms.ModelForm):
    class Meta:
        model = DeviceWishlist
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class DeviceBasketCreateForm(forms.ModelForm):
    class Meta:
        model = DeviceBasket
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class DeviceBasketUpdateForm(forms.ModelForm):
    class Meta:
        model = DeviceBasket
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class DeviceOrderCreateForm(forms.ModelForm):
    class Meta:
        model = DeviceOrder
        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state',
            Submit('submit', 'Create')
        )

class DeviceOrderUpdateForm(forms.ModelForm):
    class Meta:
        model = DeviceOrder
        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state',
            Submit('submit', 'Create')
        )

class DeviceLocalStoreCreateForm(forms.ModelForm):
    class Meta:
        model = DeviceLocalStore
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location',
            Submit('submit', 'Create')
        )

class DeviceLocalStoreUpdateForm(forms.ModelForm):
    class Meta:
        model = DeviceLocalStore
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location',
            Submit('submit', 'Create')
        )

class DeviceSetupInstanceCreateForm(forms.ModelForm):
    class Meta:
        model = DeviceSetupInstance
        fields = (
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'literature',
                'description',
                'device_setup')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'literature',
                'description',
                'device_setup',
            Submit('submit', 'Create')
        )

class DeviceSetupInstanceUpdateForm(forms.ModelForm):
    class Meta:
        model = DeviceSetupInstance
        fields = (
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'literature',
                'description',
                'device_setup')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'literature',
                'description',
                'device_setup',
            Submit('submit', 'Create')
        )

class DeviceGroupCreateForm(forms.ModelForm):
    class Meta:
        model = DeviceGroup
        fields = (
                'namespace',
                'name_full',
                'colour',
                'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'colour',
                'description',
            Submit('submit', 'Create')
        )

class DeviceGroupUpdateForm(forms.ModelForm):
    class Meta:
        model = DeviceGroup
        fields = (
                'namespace',
                'name_full',
                'colour',
                'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'colour',
                'description',
            Submit('submit', 'Create')
        )

class LabwareStateCreateForm(forms.ModelForm):
    class Meta:
        model = LabwareState
        fields = (
                'state',
                'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'state',
                'description',
            Submit('submit', 'Create')
        )

class LabwareStateUpdateForm(forms.ModelForm):
    class Meta:
        model = LabwareState
        fields = (
                'state',
                'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'state',
                'description',
            Submit('submit', 'Create')
        )

class LabwareInstanceCreateForm(forms.ModelForm):
    class Meta:
        model = LabwareInstance
        fields = (
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'labware',
                'labware_state',
                'discarded',
                'literature',
                'image')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'labware',
                'labware_state',
                'discarded',
                'literature',
                'image',
            Submit('submit', 'Create')
        )

class LabwareInstanceUpdateForm(forms.ModelForm):
    class Meta:
        model = LabwareInstance
        fields = (
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'labware',
                'labware_state',
                'discarded',
                'literature',
                'image')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'labware',
                'labware_state',
                'discarded',
                'literature',
                'image',
            Submit('submit', 'Create')
        )

class LabwareOrderItemCreateForm(forms.ModelForm):
    class Meta:
        model = LabwareOrderItem
        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'labware')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'labware',
            Submit('submit', 'Create')
        )

class LabwareOrderItemUpdateForm(forms.ModelForm):
    class Meta:
        model = LabwareOrderItem
        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'labware')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'labware',
            Submit('submit', 'Create')
        )

class LabwareWishlistCreateForm(forms.ModelForm):
    class Meta:
        model = LabwareWishlist
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class LabwareWishlistUpdateForm(forms.ModelForm):
    class Meta:
        model = LabwareWishlist
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class LabwareBasketCreateForm(forms.ModelForm):
    class Meta:
        model = LabwareBasket
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class LabwareBasketUpdateForm(forms.ModelForm):
    class Meta:
        model = LabwareBasket
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class LabwareOrderCreateForm(forms.ModelForm):
    class Meta:
        model = LabwareOrder
        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state',
            Submit('submit', 'Create')
        )

class LabwareOrderUpdateForm(forms.ModelForm):
    class Meta:
        model = LabwareOrder
        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state',
            Submit('submit', 'Create')
        )

class LabwareLocalStoreCreateForm(forms.ModelForm):
    class Meta:
        model = LabwareLocalStore
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location',
            Submit('submit', 'Create')
        )

class LabwareLocalStoreUpdateForm(forms.ModelForm):
    class Meta:
        model = LabwareLocalStore
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location',
            Submit('submit', 'Create')
        )

class LabwareGroupCreateForm(forms.ModelForm):
    class Meta:
        model = LabwareGroup
        fields = (
                'namespace',
                'name_full',
                'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'description',
            Submit('submit', 'Create')
        )

class LabwareGroupUpdateForm(forms.ModelForm):
    class Meta:
        model = LabwareGroup
        fields = (
                'namespace',
                'name_full',
                'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'description',
            Submit('submit', 'Create')
        )

class DeviceBookingCalendarCreateForm(forms.ModelForm):
    class Meta:
        model = DeviceBookingCalendar
        fields = (
                'title',
                'calendar_URL',
                'summary',
                'description',
                'color',
                'ics',
                'start_datetime',
                'end_datetime',
                'duration',
                'all_day',
                'created',
                'last_modified',
                'timestamp',
                'location',
                'geolocation',
                'conference_URL',
                'range',
                'related',
                'role',
                'tzid',
                'offset_UTC',
                'alarm_repeat_JSON',
                'event_repeat',
                'event_repeat_JSON',
                'user',
                'device')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'title',
                'calendar_URL',
                'summary',
                'description',
                'color',
                'ics',
                'start_datetime',
                'end_datetime',
                'duration',
                'all_day',
                'created',
                'last_modified',
                'timestamp',
                'location',
                'geolocation',
                'conference_URL',
                'range',
                'related',
                'role',
                'tzid',
                'offset_UTC',
                'alarm_repeat_JSON',
                'event_repeat',
                'event_repeat_JSON',
                'user',
                'device',
            Submit('submit', 'Create')
        )

class DeviceBookingCalendarUpdateForm(forms.ModelForm):
    class Meta:
        model = DeviceBookingCalendar
        fields = (
                'title',
                'calendar_URL',
                'summary',
                'description',
                'color',
                'ics',
                'start_datetime',
                'end_datetime',
                'duration',
                'all_day',
                'created',
                'last_modified',
                'timestamp',
                'location',
                'geolocation',
                'conference_URL',
                'range',
                'related',
                'role',
                'tzid',
                'offset_UTC',
                'alarm_repeat_JSON',
                'event_repeat',
                'event_repeat_JSON',
                'user',
                'device')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'title',
                'calendar_URL',
                'summary',
                'description',
                'color',
                'ics',
                'start_datetime',
                'end_datetime',
                'duration',
                'all_day',
                'created',
                'last_modified',
                'timestamp',
                'location',
                'geolocation',
                'conference_URL',
                'range',
                'related',
                'role',
                'tzid',
                'offset_UTC',
                'alarm_repeat_JSON',
                'event_repeat',
                'event_repeat_JSON',
                'user',
                'device',
            Submit('submit', 'Create')
        )

# from .forms import ExtraDataCreateForm, PartInstanceCreateForm, PartOrderItemCreateForm, PartWishlistCreateForm, PartBasketCreateForm, PartsOrderCreateForm, PartLocalStoreCreateForm, DeviceInstanceCreateForm, DeviceOrderItemCreateForm, DeviceWishlistCreateForm, DeviceBasketCreateForm, DeviceOrderCreateForm, DeviceLocalStoreCreateForm, DeviceSetupInstanceCreateForm, DeviceGroupCreateForm, LabwareStateCreateForm, LabwareInstanceCreateForm, LabwareOrderItemCreateForm, LabwareWishlistCreateForm, LabwareBasketCreateForm, LabwareOrderCreateForm, LabwareLocalStoreCreateForm, LabwareGroupCreateForm, DeviceBookingCalendarCreateFormExtraDataUpdateForm, PartInstanceUpdateForm, PartOrderItemUpdateForm, PartWishlistUpdateForm, PartBasketUpdateForm, PartsOrderUpdateForm, PartLocalStoreUpdateForm, DeviceInstanceUpdateForm, DeviceOrderItemUpdateForm, DeviceWishlistUpdateForm, DeviceBasketUpdateForm, DeviceOrderUpdateForm, DeviceLocalStoreUpdateForm, DeviceSetupInstanceUpdateForm, DeviceGroupUpdateForm, LabwareStateUpdateForm, LabwareInstanceUpdateForm, LabwareOrderItemUpdateForm, LabwareWishlistUpdateForm, LabwareBasketUpdateForm, LabwareOrderUpdateForm, LabwareLocalStoreUpdateForm, LabwareGroupUpdateForm, DeviceBookingCalendarUpdateForm
